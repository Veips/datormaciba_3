#include <stdio.h>

 int main()
 {
 long int num , i; // num -- skaitlis
 long int reizinajums = 1; // faktorials no 0! = 1

 printf("Ievadam veselu skaitli: ");
 scanf("%ld", &num);

 for ( i=1 ; i<=num ; i++ )
 {
 reizinajums *= i;
 } // Izvadee ir kljuuda. Labo to!

printf("Skaitlja %ld faktorials ir: %ld\n", num, reizinajums);
}
