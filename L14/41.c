/* Programma 40. c -- Masiiva kaartoshana */
#include <stdio.h>
#include <stdlib.h>
#define max(a, b) (a > b ? a : b)
#define min(a, b) (a < b ? a : b)

int main()
{
int i, j, k; // ciklu mainigie
int Temp; // iislaiciigais mainiigais
int mas3[] = {1, 7, 2, -2, 10, 33, 1, 1, 1, 1, 3, 8, 4, 5, 6, 7}; // kaartojamo skaitlju masiivs
int arrSize = (sizeof(mas3)/ sizeof(int));
printf ("Masiiva izmeers = %d\n", arrSize);

for (i=0; i<arrSize; i++) // cikliska masiiva kaartoshana ar burbuli
{
for (j=0; j<(arrSize-1); j++) // nakoshais cikls ir
{ // par vienu mazaaks
if (mas3[j] > mas3[j+1])
{
Temp = mas3[j];
mas3[j] = mas3[j+1];
mas3[j+1] = Temp;
}
}
}
// sakaartotaa masiiva izvade uz ekrana

int minv = 1000;
int maxv = -1000;
float avg = 0;
for(k=0; k<(arrSize); k++) {
	printf("%d,",mas3[k]);
avg += mas3[k];
minv = min(minv, mas3[k]);
maxv = max(maxv, mas3[k]);
}
avg /= arrSize;

printf("\n");
printf("Kopas elementu skaits: %d\n", arrSize);
printf("Max %d\nMin %d\n", maxv, minv);
printf("Avg %f\n", avg);

if((sizeof(mas3)/sizeof(mas3[0])) % 2 == 0) {
printf("Mediana %f\n", (mas3[arrSize / 2-1] + mas3[arrSize / 2]) / 2.0f);
} else {
printf("Mediana %d\n", mas3[arrSize / 2]);
}


}





