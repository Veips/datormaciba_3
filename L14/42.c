/* Programma 40. c -- Masiiva kaartoshana */
#include <stdio.h>
#include <stdlib.h>
#define max(a, b) (a > b ? a : b)
#define min(a, b) (a < b ? a : b)

int main()
{
int i, j, k; // ciklu mainigie
int Temp; // iislaiciigais mainiigais
char mas3[] = "Rihards Veips"; // kaartojamo skaitlju masiivs
int arrSize = (sizeof(mas3)/ sizeof(mas3[0]));
printf ("Masiiva izmeers = %d\n", arrSize);

for (i=0; i<arrSize; i++) // cikliska masiiva kaartoshana ar burbuli
{
for (j=0; j<(arrSize-1); j++) // nakoshais cikls ir
{ // par vienu mazaaks
if (mas3[j] > mas3[j+1])
{
Temp = mas3[j];
mas3[j] = mas3[j+1];
mas3[j+1] = Temp;
}
}
}

for(k=0; k<(arrSize); k++) {
	printf("0x%02X = \"%c\"\n",mas3[k], mas3[k]);
}
printf("\n");

}





