#include <stdio.h>
#include <math.h>

int main() {
	for(int k = 1; k <= 50; k++) {
		double lim = pow((1+1./k), k);
		printf("k = %d lim = %f\n", k, lim);
	}
}
