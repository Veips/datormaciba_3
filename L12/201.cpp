#include <iostream>
using namespace std;

int main() {
	double deg;
	cout << "Deg: ";
	cin >> deg;
	printf("%.2f deg = %.3f rad\n", deg, deg * 0.0174532925);
}
